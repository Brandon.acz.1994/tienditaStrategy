/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

/**
 *
 * @author Brandon
 */
public abstract class Product {
    
    private double preciounitario;
    private double cantidad;
    
    public Product(double preciounitario){
    this.preciounitario=preciounitario;    
    }
    
    public void setCosto(double preciounitario) {
       this.preciounitario=preciounitario;
    }
   
    public void setCantidad(double cantidad){
      this.cantidad=cantidad;
    
    }
    
    public double getCosto() {
        Double costo=cantidad*preciounitario;
        return costo ;
    }
    
}
