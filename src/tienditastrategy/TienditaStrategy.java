/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tienditastrategy;

import product.ProductoFruta;
import product.ProductoLacteo;


/**
 *
 * @author Brandon
 */
public class TienditaStrategy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      ProductoLacteo leche = new ProductoLacteo(15.50);
      ProductoLacteo crema = new ProductoLacteo(10);
      ProductoLacteo mantequilla = new ProductoLacteo(4.50);
      ProductoLacteo queso = new ProductoLacteo(100);
      ProductoFruta platanos = new ProductoFruta(14);
      ProductoFruta manzana= new ProductoFruta(13);
      
    
      
      VentaLunes venta1 = new VentaLunes();      
      VentaMartes venta2 = new VentaMartes(); 
      VentaMiercoles venta3 = new VentaMiercoles();
      VentaJueves venta4 = new VentaJueves();
      VentaViernes venta5 = new VentaViernes();
      VentaSabado venta6 = new VentaSabado();  
      VentaDomingo venta7 = new VentaDomingo(); 
      
       leche.setCantidad(10);
       venta1.addProduct(leche);
       venta2.addProduct(leche);
       venta3.addProduct(leche);
       venta4.addProduct(leche);
       venta5.addProduct(leche);
       venta6.addProduct(leche);
       venta7.addProduct(leche);
       
       crema.setCantidad(0.5);
       venta1.addProduct(crema);
       venta2.addProduct(crema);
       venta3.addProduct(crema);
       venta4.addProduct(crema);
       venta5.addProduct(crema);
       venta6.addProduct(crema);
       venta7.addProduct(crema);
       
       mantequilla.setCantidad(.250);
       venta1.addProduct(mantequilla);
       venta2.addProduct(mantequilla);
       venta3.addProduct(mantequilla);
       venta4.addProduct(mantequilla);
       venta5.addProduct(mantequilla);
       venta6.addProduct(mantequilla);
       venta7.addProduct(mantequilla);
       
       queso.setCantidad(.500);
       venta1.addProduct(queso);
       venta2.addProduct(queso);
       venta3.addProduct(queso);
       venta4.addProduct(queso);
       venta5.addProduct(queso);
       venta6.addProduct(queso);
       venta7.addProduct(queso);
       
       platanos.setCantidad(1);
       venta1.addProduct(platanos);
       venta2.addProduct(platanos);
       venta3.addProduct(platanos);
       venta4.addProduct(platanos);
       venta5.addProduct(platanos);
       venta6.addProduct(platanos);
       venta7.addProduct(platanos);
       
       manzana.setCantidad(2);
       venta1.addProduct(manzana);
       venta2.addProduct(manzana);
       venta3.addProduct(manzana);
       venta4.addProduct(manzana);
       venta5.addProduct(manzana);
       venta6.addProduct(manzana);
       venta7.addProduct(manzana);
      
      System.out.println("venta un dia lunes");
      venta1.TotalVenta();
       System.out.println("venta un dia martes");
      venta2.TotalVenta();
       System.out.println("venta un dia miercoles");
      venta3.TotalVenta();
       System.out.println("venta un dia jueves");
      venta4.TotalVenta();
       System.out.println("venta un dia viernes");
      venta5.TotalVenta();
       System.out.println("venta un dia sabado");
      venta6.TotalVenta();
       System.out.println("venta un dia domingo");
      venta7.TotalVenta();
        
      
      
      
      
      
      
     
      
    }
    
}
