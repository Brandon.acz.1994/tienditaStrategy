/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tienditastrategy;

import java.text.DecimalFormat;
import java.util.ArrayList;
import promotion.PromotionBehavior;
import product.Product;


/**
 *
 * @author Brandon
 */
public abstract class Venta  {
    
    protected PromotionBehavior promotionbehavior;  
    protected ArrayList<Product> productslist= new ArrayList<>();
    DecimalFormat numberFormat = new DecimalFormat("#.00");
    private double total;
    
    public Venta(){
      
    }
    
    public void setPromotionBehavior(PromotionBehavior pb){        
    promotionbehavior =pb;
    
    }
    
    public void perfomPromotionBehavior(){
    double descontar= promotionbehavior.Descuento(productslist);
    total=total-descontar;
    }
    
    public void addProduct(Product product){        
    productslist.add(product);

    
    }
    
    public void Suma(){             
    productslist.stream().forEach(product -> {
    total=total+product.getCosto();
      
     });
          
    }    
        
    public void Impuesto(){
    total=(total+(total*(0.16)));    
    }
    
    public void TotalVenta(){
    
    Suma();
    System.out.println("Suma:  $"+numberFormat.format(total));
    Impuesto();
    System.out.println("Mas impuestos:  $"+numberFormat.format(total));
    perfomPromotionBehavior();
    System.out.println("Total:  $"+numberFormat.format(total));
    System.out.println();
    }
   
    
}
    


