/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package promotion;

import java.util.ArrayList;
import product.ProductoEmbutido;
import product.ProductoLacteo;
import product.Product;

/**
 *
 * @author Brandon
 */
public class PromoEmbutidosLacteos implements PromotionBehavior {
   
     private double descontar;

    @Override
    public double Descuento(ArrayList<Product> productslist) {
      productslist.stream().forEach(product ->{
      
      if(product instanceof ProductoLacteo){
     descontar=descontar+(product.getCosto()*0.15);
      }
      
      if(product instanceof ProductoEmbutido){
     descontar=descontar+(product.getCosto()*0.05);
      }
          
      });
    System.out.println("se realiza descuento de 15% en lacteos y 5% en embutidos");
    return  descontar;  
    }
    
}
